import django
import tweepy
from datetime import datetime
import logging
from config import create_api
import time
import os
import schedule

#to Stop the Django server
from subprocess import Popen
from signal import SIGINT
# import Django models

# import time

# def job():
#open django settings
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'oil_industry_awareness.settings')

django.setup()

from oil_tweets.models import Tweep, Tweet

#Authenticate yourself to Twitter
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

#set counter
for x in range(1):
    #Query the Twitter API
    def main():
        api = create_api()
        # while True:
        for tweet in api.search(q="(oil gas uganda) -is:retweet", lang="en", rpp=10):
          
            #Input the twitter data inside the Django models

            dataset = Tweep.objects.create(author_id=tweet.user.id, name=tweet.user.name, start_date=tweet.user.created_at.strftime(
                '%Y-%m-%dT%H:%M:%S.%f%z'), verified=tweet.user.verified)

            if (dataset):
                print("Tweep has been created")
            else:
                print("Something went wrong")

            time.sleep(1)

            tweet_row = Tweet.objects.create(Tweep=dataset, text=tweet.text, created_at=tweet.created_at.strftime(
                '%Y-%m-%dT%H:%M:%S.%f%z'), device=tweet.source)
            if(tweet_row):
                print("Tweet has been saved")
            else:
                print("Tweet wasn't saved")


    if __name__ == "__main__":
        main()


    time.sleep(2)
    x+=1
    print( "I have scratched the twitter api ")
    print(x)
    print("times now")
    if(x == 1):
        print("That's enough for now")
        quit()



# start the process
p = Popen(['python', 'manage.py', 'runserver'])

# now stop the process
p.send_signal(SIGINT)
p.wait()
# # schedule.every(1).minutes.do(job)
# schedule.every(3).seconds.do(job)


# while True:
#     schedule.run_pending()
#     print("Job Done")
#     time.sleep(5)

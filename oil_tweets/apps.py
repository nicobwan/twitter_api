from django.apps import AppConfig


class OilTweetsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'oil_tweets'

from django.urls import path

from oil_tweets import views

urlpatterns = [
    path('', views.index, name='index'),
]
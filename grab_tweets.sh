# Activate virtualenv
/home/off-duty-manager/.virtualenvs/tweepy/bin/source .virtualenvs/tweepy/bin/activate

#run server
/home/off-duty-manager/.virtualenvs/tweepy/bin/python code/oil_and_gas_tweets/oil_industry_awareness/manage.py

# run grab the tweet automatically
/home/off-duty-manager/.virtualenvs/tweepy/bin/python code/oil_and_gas_tweets/oil_industry_awareness/tweet_aware_input.py
touch code/oil_and_gas_tweets/oil_industry_awareness/done.txt